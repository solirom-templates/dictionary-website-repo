const toml = require("toml");

module.exports = eleventyConfig => {
  eleventyConfig.addDataExtension("toml", contents => toml.parse(contents));
  eleventyConfig.setTemplateFormats("11ty.js,mjs,css,ico");
  eleventyConfig.addPassthroughCopy("template/images");
  eleventyConfig.addPassthroughCopy("template/robots.txt");
  eleventyConfig.addPassthroughCopy({ "src/images": "images" });
  eleventyConfig.addPassthroughCopy({ "src/*.html": "/" });

  return {
    dir: {
      input: "template",
      data: "../src",
      output: "public"
    }     
  }  
};
